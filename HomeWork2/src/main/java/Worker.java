import animals.Animal;
import animals.actions.Voice;
import food.Food;

public class Worker {

    private String name;
    private int age;

    public Worker(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void feed(Animal animal, Food food) {
        System.out.println(this.name + " feed animal");
        animal.eat(food);
    }

    public void getVoice(Voice animal) {
        System.out.println(this.name + " asks" + " get voice");
        System.out.println(animal.voice());
    }
}
