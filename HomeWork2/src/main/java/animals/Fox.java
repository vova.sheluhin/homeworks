package animals;

import animals.actions.Run;
import animals.actions.Swim;
import animals.actions.Voice;

public class Fox extends Carnivorous implements Run, Swim, Voice {

    public Fox(String name, int weight, int age) {
        super.setName(name);
        super.setWeight(weight);
        super.setAge(age);
    }

    @Override
    public void run() {
        System.out.println("Fox " + this.getName() + " begin run");
    }

    @Override
    public void swim() {
        System.out.println("Fox " + this.getName() + " begin swim");
    }

    @Override
    public String voice() {
        return "Fox " + this.getName() + " voice \"Wa-pa-pa-pa-pa-pa-pow!\n";  // What Does The Fox Say?

    }
}
