package animals;

import animals.actions.Swim;

public class Fish extends Carnivorous implements Swim {

    public Fish(String name, int weight, int age) {
        super.setName(name);
        super.setWeight(weight);
        super.setAge(age);
    }

    @Override
    public void swim() {
        System.out.println("Fish " + this.getName() + " begin swim");
    }
}
