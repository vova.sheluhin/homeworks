package animals;

import animals.actions.Run;
import animals.actions.Swim;
import animals.actions.Voice;

public class Horse extends Herbivore implements Run, Swim, Voice {

    public Horse(String name, int weight, int age) {
        super.setName(name);
        super.setWeight(weight);
        super.setAge(age);
    }

    @Override
    public void run() {
        System.out.println("Horse " + this.getName() + " begin run");
    }

    @Override
    public void swim() {
        System.out.println("Horse " + this.getName() + " begin run");
    }

    @Override
    public String voice() {
        return "Horse " + this.getName() + " voice \"neigh\"";
    }
}
