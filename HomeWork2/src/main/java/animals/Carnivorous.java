package animals;

import food.Food;
import food.Grass;

public abstract class Carnivorous extends Animal {
    @Override
    public void eat(Food food) {
        if (food instanceof Grass) {
            System.out.println("Fox " + this.getName() + " not eat grass");
        } else System.out.println("Fox " + this.getName() + " begin eat " + food.getFoodName());
    }
}
