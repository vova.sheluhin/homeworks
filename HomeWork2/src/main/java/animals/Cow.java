package animals;

import animals.actions.Run;
import animals.actions.Swim;
import animals.actions.Voice;

public class Cow extends Herbivore implements Run, Swim, Voice {

    public Cow(String name, int weight, int age) {
        super.setName(name);
        super.setWeight(weight);
        super.setAge(age);
    }


    @Override
    public void run() {
        System.out.println("Cow " + this.getName() + " begin run");
    }

    @Override
    public String voice() {
        return "Cow " + this.getName() + " voice \"Moooo\"";
    }

    @Override
    public void swim() {
        System.out.println("Cow " + this.getName() + " begin swim");
    }
}
