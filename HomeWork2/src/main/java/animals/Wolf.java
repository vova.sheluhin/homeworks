package animals;

import animals.actions.Run;
import animals.actions.Swim;
import animals.actions.Voice;

public class Wolf extends Carnivorous implements Run, Swim, Voice {

    public Wolf(String name, int weight, int age) {
        super.setName(name);
        super.setWeight(weight);
        super.setAge(age);
    }

    @Override
    public void run() {
        System.out.println("Wolf " + this.getName() + " begin run");
    }

    @Override
    public void swim() {
        System.out.println("Wolf " + this.getName() + " begin run");
    }

    @Override
    public String voice() {
        return "Wolf " + this.getName() + " voice \"Wooo-oo-oooo-ooooo\"";
    }
}
