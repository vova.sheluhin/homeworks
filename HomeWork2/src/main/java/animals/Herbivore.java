package animals;

import food.Food;
import food.Meat;

public abstract class Herbivore extends Animal {
    @Override
    public void eat(Food food) {

        if (food instanceof Meat) {
            System.out.println(getName() + " " + this.getName() + " not eat meat");
        } else {
            System.out.println(getName() + " " + this.getName() + " begin eat " + food.getFoodName());
            setSatiety(getSatiety() + food.getSatiety());
        }
    }
}
