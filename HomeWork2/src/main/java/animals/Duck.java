package animals;

import animals.actions.Fly;
import animals.actions.Run;
import animals.actions.Swim;
import animals.actions.Voice;

public class Duck extends Herbivore implements Fly, Run, Swim, Voice {

    public Duck(String name, int weight, int age) {
        super.setName(name);
        super.setWeight(weight);
        super.setAge(age);
    }

    @Override
    public void fly() {
        System.out.println("Duck " + this.getName() + " begin fly");
    }

    @Override
    public void run() {
        System.out.println("Duck " + this.getName() + " begin run");
    }

    @Override
    public void swim() {
        System.out.println("Duck " + this.getName() + " begin swim");
    }

    @Override
    public String voice() {
        return "Duck " + this.getName() + " voice \"Krya-krya\"";
    }
}
