package food;

import food.Meat;

public class FishMeat extends Meat{

    public FishMeat(String meatName) {
        super(meatName);
        setSatiety(5);
    }
}
