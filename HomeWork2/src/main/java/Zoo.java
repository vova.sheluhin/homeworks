import animals.*;
import food.FishMeat;
import food.Food;
import food.Grass;
import food.WheatGrass;

public class Zoo {

    public static void main(String[] args) {
        Cow cow = new Cow("cow1", 123, 2);
        System.out.println(cow.voice());
        cow.swim();
        cow.run();
        cow.eat(new WheatGrass("grass"));
        System.out.println(cow.getSatiety());

        System.out.println();

        Fox fox = new Fox("fox1", 123, 2);
        System.out.println(fox.voice());
        fox.swim();
        fox.run();
        fox.eat(new WheatGrass("grass"));
        System.out.println(fox.getSatiety());

        System.out.println();

        Worker worker = new Worker("Name", 123);
        worker.getVoice(fox);

        worker.feed(fox, new FishMeat("catfish"));

    }
}
